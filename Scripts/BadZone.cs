using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossingPlatformer {
    public class BadZone : MonoBehaviour
    {
        //PlayerController playerController;

        private void Awake()
        {
            //playerController = FindObjectOfType<PlayerController>();
        }

        private void OnTriggerEnter(Collider other)
        {
            PlayerController playerController = other.GetComponent<PlayerController>();
            playerController.Damaged();
        }
    } 
}

