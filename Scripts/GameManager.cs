using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace CrossingPlatformer
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance;

        public PlayerControls controls;
        PlayerController playerController;
        PlayerAnimation playerAnimation;
        AudioManager audioManager;

        [Header("Loading Screen")]
        public GameObject loadingScreen;
        public Slider loadingSlider;
        public TMP_Text progressText;

        public int currentCoin;

        public Vector3 lastCheckpointPosition;

        //public Slider timeBar;
        //public TMP_Text hpDisplay;
        //public TMP_Text coinDisplay;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(instance);
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(gameObject);

            playerController = FindObjectOfType<PlayerController>();
            audioManager = FindObjectOfType<AudioManager>();
        }


        public void Restart()
        {
            LoadLevel(SceneManager.GetActiveScene().buildIndex);
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void LoadLevel(int level)
        {
            StartCoroutine(LoadAsynchronously(level));
        }

        IEnumerator LoadAsynchronously(int level)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(level);

            loadingScreen.SetActive(true);

            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / .9f);

                //Debug.Log(progress);
                loadingSlider.value = progress;
                progressText.text = Mathf.Round(progress * 100) + "%";

                yield return null;
            }
        }


        public void AddCoin(int coin)
        {
            currentCoin += coin;
            
        }

    }
}
