using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossingPlatformer
{
    public class PlayerAnimation : MonoBehaviour
    {
        public Animator animator;
        Rigidbody rigidBody;
        const float locomationAnimationSmoothTime = .1f;
        AudioManager audioManager;


        private void Awake()
        {
            animator = GetComponent<Animator>();
            rigidBody = GetComponentInParent<Rigidbody>();
            audioManager = FindObjectOfType<AudioManager>();
        }

        private void Update()
        {
            animator.SetFloat("moveAmount", rigidBody.velocity.normalized.magnitude, locomationAnimationSmoothTime, Time.deltaTime);
        }

        public void PlayAnimation(string animName)
        {
            animator.Play(animName);
        }

        public void PlayStepSound (int index)
        {
            if (index == 1)
                audioManager.Play("Step1");
            else if (index == 2)
                audioManager.Play("Step2");
        }
    }
}
