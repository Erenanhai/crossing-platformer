using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossingPlatformer
{
    public class CoinManager : Interactible
    {
        public int coinValue;

        public GameObject pickupEffect;

        AudioManager audioManager;

        private void Awake()
        {
            audioManager = FindObjectOfType<AudioManager>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                audioManager.Play("Coin");
                GameObject effect = Instantiate(pickupEffect, transform.position, transform.rotation);
                FindObjectOfType<GameManager>().AddCoin(coinValue);

                Destroy(effect, 1);
                Destroy(gameObject);
            }
        }
    }
}
