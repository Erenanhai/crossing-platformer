using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossingPlatformer
{
    public class Checkpoint : MonoBehaviour
    {
        private GameManager gameManager;
        AudioManager audioManager;
        [SerializeField] GameObject door;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
            audioManager = FindObjectOfType<AudioManager>();
        }

        private void Start()
        {
            door.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                gameManager.lastCheckpointPosition = transform.position;
                audioManager.Play("Checkpoint");
                door.SetActive(true);
            }
        }
    }
}
