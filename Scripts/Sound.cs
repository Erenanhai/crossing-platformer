using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    [Header("Audio Setting")]
    [Range(0, 1)]
    public float volume;
    [Range(.1f, 3f)]
    public float pitch;

    [Range(0, 1)]
    public float spatialBlend;

    public bool loop;

    [HideInInspector]
    public AudioSource source;
}
