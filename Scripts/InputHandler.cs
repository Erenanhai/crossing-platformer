using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CrossingPlatformer
{
    public class InputHandler : MonoBehaviour
    {
        public PlayerControls controls;
        PlayerController playerController;
        GameManager gameManager;

        private void Awake()
        {
            playerController = FindObjectOfType<PlayerController>();
            gameManager = FindObjectOfType<GameManager>();
        }

        private void OnEnable()
        {
            if (controls == null)
            {
                controls = new PlayerControls();
                //controls.PlayerMovement.Jump.performed += playerJump => Jump();
                controls.PlayerMovement.Jump.performed += playerJump => playerController.isJumping = true;
                controls.PlayerMovement.Jump.canceled += playerMiniJump => playerController.MiniJump();

                controls.PlayerMovement.Movement.performed += playerMove => playerController.GetMoveInput(playerMove.ReadValue<Vector2>());

                controls.PlayerMovement.Dash.performed += playerDash => playerController.dashTrigger = true;

                //controls.PlayerAction.Punch.performed += playerPunch => playerController.Punch();


                //controls.PlayerAction.TimeControl.performed += slow => playerController.TimeControl();
                //controls.PlayerAction.TimeControl.canceled += normal => playerController.GoTime();

                controls.GameAction.Quit.performed += exit => gameManager.Quit();
            }

            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }
    }
}
