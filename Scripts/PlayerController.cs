using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace CrossingPlatformer
{
    public class PlayerController : MonoBehaviour
    {
        public PlayerControls controls;
        PlayerAnimation playerAnimation;
        AudioManager audioManager;
        GameManager gameManager;

        [HideInInspector]
        public Rigidbody rigidBody;

        public Transform cam;

        //Vector3 moveDirection;
        float moveDirectionX;
        float moveDirectionZ;
        Vector3 moveDir;

        [Header("Ground")]
        [SerializeField]
        Transform groundCheck;
        [SerializeField]
        LayerMask groundCheckLayer;
        private int groundCollider;

        [Header("Jump setting")]
        [SerializeField]
        float hangTime = .3f;
        float hangCounter;

        [SerializeField]
        int multiJump = 2;
        [SerializeField]
        int jumpCount;

        [SerializeField]
        float jumpBufferLengh = .1f;
        float jumpBufferCount;

        [Header("Turn smooth")]
        public float turnSmoothTime = 0.1f;
        float turnSmoothVellocity;

        [Header("Stats")]
        [SerializeField]
        float movementSpeed = 5;
        [SerializeField]
        float acceleration = 0.25f;
        [SerializeField]
        float jumpHight = 5;
        [SerializeField]
        float dashPower = 10;
        public int maxHeart = 3;
        [HideInInspector]
        public int heartRemain;

        
        float damageTimeCounter;
        [SerializeField]
        float DamageCooldown = 6;

        bool isDead;

        
        public float dashTime = 0.5f;
        public float dashCooldown = 5;
        //[HideInInspector]
        //public float dashCounter;

        [Header("Slow-mo")]
        public float slowMotionAmount;
        public float slowMotionTime;
        public float slowMotionLimit = 3;

        // flags
        //bool isInteracting;
        [HideInInspector]
        public bool isJumping;
        [HideInInspector]
        public bool isDashing;
        [HideInInspector]
        public bool canDash;
        [HideInInspector]
        public bool dashTrigger;

        [Header("Effects")]
        public ParticleSystem footSteps;
        private ParticleSystem.EmissionModule footEmission;
        [SerializeField] private TrailRenderer trail;
        public ParticleSystem landingEffect;
        private bool wasOnGround;


        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            playerAnimation = GetComponentInChildren<PlayerAnimation>();
            audioManager = FindObjectOfType<AudioManager>();
            gameManager = FindObjectOfType<GameManager>();
        }

        private void Start()
        {
            //Time.timeScale = slowMotionAmount;
            Cursor.lockState = CursorLockMode.Locked;

            damageTimeCounter = DamageCooldown;
            isDead = false;

            heartRemain = maxHeart;

            slowMotionTime = slowMotionLimit * 2;
            footEmission = footSteps.emission;

            isJumping = false;
            canDash = true;
            isDashing = false;
            dashTrigger = false;

            transform.position = gameManager.lastCheckpointPosition;
        }

        private void Update()
        {
            //if (isDashing)
            //    return;

            if (isDead)
                return;

            // Jump Buffer
            if (isJumping)
            {
                jumpBufferCount = jumpBufferLengh;
            }
            else
            {
                jumpBufferCount -= Time.deltaTime;
            }

            // Hangtime
            if (groundCollider > 0)
            {
                hangCounter = hangTime;
            }
            else
            {
                hangCounter -= Time.deltaTime;
            }

            groundCollider = Physics.OverlapSphere(groundCheck.position, .01f, groundCheckLayer).Length;

            if (jumpCount == 0 && groundCollider > 0)
            {
                jumpCount = multiJump;
            }

            slowMotionTime += Time.deltaTime;
            damageTimeCounter += Time.deltaTime;

            if ((moveDirectionX != 0 || moveDirectionZ != 0) && groundCollider > 0)
            {
                footEmission.rateOverTime = 35;
            }
            else
            {
                footEmission.rateOverTime = 0;
            }

            if(!wasOnGround && groundCollider > 0)
            {
                landingEffect.gameObject.SetActive(true);
                landingEffect.Stop();
                landingEffect.transform.position = footSteps.transform.position;
                landingEffect.Play();
                audioManager.Play("Land");
            }

            if (groundCollider > 0)
            {
                playerAnimation.animator.SetBool("isGrounded", true);
                wasOnGround = true;
            }
            else
            {
                playerAnimation.animator.SetBool("isGrounded", false);
                wasOnGround = false;
            }

            if(rigidBody.transform.position.y <= -10)
            {
                StartCoroutine(Dead());
            }
        }

        private void FixedUpdate()
        {
            if (isDead)
                return;

            if (dashTrigger && canDash)
                StartCoroutine(Dash());
            else
                Move();

            if (isJumping)
            {
                Jump();
                isJumping = false;
            }

            if ((Mathf.Abs(moveDirectionX) == 0 && Mathf.Abs(moveDirectionZ) == 0) && slowMotionTime <= slowMotionLimit)
                SlowTime();
            else if (Mathf.Abs(moveDirectionX) >= .1f || Mathf.Abs(moveDirectionZ) >= .1f)
                GoTime();
        }

        #region Locomotion
        #region Move;
        /// <summary>
        /// Get move direction
        /// </summary>
        /// <param name="direction"></param>
        public void GetMoveInput(Vector2 direction)
        {
            //Debug.Log(direction);
            moveDirectionX = direction.x;
            moveDirectionZ = direction.y;

        }

        /// <summary>
        /// Handle movement
        /// </summary>
        public void Move()
        {

            if (Mathf.Abs(moveDirectionX) >= .1f || Mathf.Abs(moveDirectionZ) >= .1f)
            {
                //GoTime();
                float targetAngle = Mathf.Atan2(moveDirectionX, moveDirectionZ) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVellocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward * movementSpeed;
                if (!isDashing)
                    rigidBody.velocity = new Vector3(moveDir.x, GetComponent<Rigidbody>().velocity.y, moveDir.z);
            }
            else if (Mathf.Abs(moveDirectionX) == 0 && Mathf.Abs(moveDirectionZ) == 0)
            {
                //SlowTime();

                rigidBody.velocity = new Vector3(rigidBody.velocity.x * acceleration, GetComponent<Rigidbody>().velocity.y, rigidBody.velocity.z * acceleration);
            }
        }
        #endregion

        #region Jump
        /// <summary>
        /// Handle Jumping
        /// </summary>
        public void Jump()
        {
            if (hangCounter > 0 && jumpBufferCount >= 0)
            {
                playerAnimation.PlayAnimation("Jump");
                audioManager.Play("Jump");
                //rigidBody.AddForce(Vector3.up * jumpHight, ForceMode.VelocityChange);
                rigidBody.velocity = new Vector3(rigidBody.velocity.x, jumpHight, rigidBody.velocity.z);
                jumpBufferCount = 0;
            }


        }
        public void MiniJump()
        {
            if (rigidBody.velocity.y > 0)
            {
                rigidBody.velocity = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y * 0.5f, rigidBody.velocity.z);
            }

        }
        #endregion

        #region Dash
        /// <summary>
        /// Handle Dashing
        /// </summary>
        public IEnumerator Dash()
        {
            canDash = false;
            isDashing = true;

            rigidBody.useGravity = false;

            //rigidBody.AddForce(moveDir.normalized * dashPower, ForceMode.Impulse);
            rigidBody.velocity = moveDir.normalized * dashPower;
            trail.emitting = true;
            audioManager.Play("Dash");
            yield return new WaitForSeconds(dashTime);

            trail.emitting = false;
            rigidBody.useGravity = true;
            isDashing = false;
            dashTrigger = false;

            yield return new WaitForSeconds(dashCooldown);
            canDash = true;
        }
        #endregion

        #endregion

        #region Action
        #region Time
        /// <summary>
        /// Slow time scale
        /// </summary>
        public void SlowTime()
        {
            Time.timeScale = slowMotionAmount;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }

        /// <summary>
        /// Set time scale to normal
        /// </summary>
        public void GoTime()
        {
            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f;
        }

        /// <summary>
        /// Handle time control with cooldown
        /// </summary>
        public void TimeControl()
        {
            if (slowMotionTime >= slowMotionLimit * 2)
            {
                slowMotionTime = 0;
            }
            else if (slowMotionTime <= slowMotionLimit)
            {
                slowMotionTime = slowMotionLimit;
                GoTime();
            }
        }
        #endregion

        public void Punch()
        {
            playerAnimation.PlayAnimation("Punch");
        }

        /// <summary>
        /// Handle damages to player
        /// </summary>
        public void Damaged()
        {
            if (damageTimeCounter > DamageCooldown)
            {
                Debug.Log("Damage!");

                heartRemain -= 1;
                damageTimeCounter = 0;
                playerAnimation.PlayAnimation("Hit");
                audioManager.Play("Hurt");
            }
            if(heartRemain <= 0)
            {
                StartCoroutine(Dead());
            }
        }

        /// <summary>
        /// Handle dead
        /// </summary>
        public IEnumerator Dead()
        {
            Debug.Log("Game over");
            playerAnimation.PlayAnimation("Death");
            audioManager.Play("Dead");
            isDead = true;
            //Destroy(gameObject, 2);

            yield return new WaitForSeconds(2);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            gameManager.Restart();
            
        }
        #endregion
    }
}
