using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //field
    private bool jumpKeyPressed;

    //private bool isGrounded;
    [SerializeField] private Transform groundCheckTransform;      //or public Transform groundCheckTransform;
    [SerializeField] private Transform cameraFollowTransform;

    private float horizontalInpX;
    private float horizontalInpZ;

    private float jumpHight;
    private float maxSpeed;
    private int jumpTime;

    private Rigidbody rigidbodyComponent;

    private int superJumpRemaining;


    // Start is called before the first frame update (best for initilizing and keeping things unchanged)
    void Start()
    {
        maxSpeed = 5;

        rigidbodyComponent = GetComponent<Rigidbody>();
    }

    // Update is called once per frame (best for getting input)
    void Update()
    {
        /**
         * send jump message if key is pressed
         */
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //jumpTime++;
            jumpKeyPressed = true;
        }

        /*
        // just messed around with the AddForce function :v

        if (Input.GetKey(KeyCode.W))
        {
            //Debug.Log("Forward");

            GetComponent<Rigidbody>().AddForce(Vector3.forward * 2, ForceMode.Force);

        }

        if (Input.GetKey(KeyCode.S))
        {
            //Debug.Log("Backward");

            GetComponent<Rigidbody>().AddForce(Vector3.back * 2, ForceMode.Force);

        }

        if (Input.GetKey(KeyCode.A))
        {
            //Debug.Log("Left");

            GetComponent<Rigidbody>().AddForce(Vector3.left * 2, ForceMode.Force);

        }

        if (Input.GetKey(KeyCode.D))
        {
            //Debug.Log("Right");

            GetComponent<Rigidbody>().AddForce(Vector3.right * 2, ForceMode.Force);

        }
        */

        horizontalInpX = Input.GetAxis("Horizontal") * maxSpeed;
        horizontalInpZ = Input.GetAxis("Vertical") * maxSpeed;
    }

    //called once every physics update (100hz or 100times/seconds)
    private void FixedUpdate()
    {
        //Debug.Log(rigidbodyComponent.velocity);
        //Debug.Log(jumpTime);

        rigidbodyComponent.velocity = new Vector3(horizontalInpX, GetComponent<Rigidbody>().velocity.y, horizontalInpZ);

        /**
         * Jump (or flap since you can multi jump :v) if get the message
         * 
         * flap: 
         *      1. jump
         *      2. set message to false
         *      
         * jump only
         *  Method 1:
         *      1. jump if currently touch on ground (check for isGround)
         *      2. set message to false
         *      
         * bugs: 
         * - if hit jump key multiple times, the character will auto-jump for the same number of time after touching the ground
         * - the character will stuck if on small gap of two objects
         * 
         *  Method 2:
         *      1. jump if currently touch on ground (check for number of colliders with ground)
         *      2. set message to false
         *      
         *  bugs:
         *  - if hit jump on the edge of the platform, the character won't jump, but will auto-jump if move back from the edge.
         */

        /*
            Note: You can also use layermask to check for numbers of colliders while skipping the collider with itself
            
            [SerializeField] private LayerMask playerMask;
            Physics.OverlapSphere(groundCheckTransform.position, 0.1f, playerMask).Length (= 0 if on air)
            
            (besure to tick everything except for player (or anything player currently carry) layer)
         */

        int withGroundColliderNumber = Physics.OverlapSphere(groundCheckTransform.position, 0.1f).Length;
        
        if (withGroundColliderNumber == 1) //the character will collide with itself so length >= 1 for each cases
        {
            
            return;
        }
        
        if (jumpKeyPressed)
        {
            //Debug.Log("Jump");

            jumpHight = 5.5f;
            //jumpTime = 0;

            if (superJumpRemaining > 0)
            {
                jumpHight *= 2;
                superJumpRemaining--;
            }

            rigidbodyComponent.AddForce(Vector3.up * jumpHight, ForceMode.VelocityChange);

            jumpKeyPressed = false;
        }

    }

    /*
    //check for collision (touch with other obj)
    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }

    //after collision (not touch anymore)
    private void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }
    */


    /**
     * Check for collision (trigger)
     */

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            superJumpRemaining++;
            Destroy(other.gameObject);
        }
    }
}
