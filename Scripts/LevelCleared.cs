using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CrossingPlatformer {
    public class LevelCleared : MonoBehaviour
    {
        public GameObject winScreen;
        GameManager gameManager;

        private void Awake()
        {
            gameManager = GetComponent<GameManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log(SceneManager.GetActiveScene().buildIndex);
                if(SceneManager.GetActiveScene().buildIndex == 1)
                    gameManager.LoadLevel(2);
                else
                    StartCoroutine(Win());
            }
        }

        public IEnumerator Win()
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            Debug.Log("Level cleared!");
            //swinScreen.SetActive(true);
            winScreen.GetComponent<Animator>().Play("Winscreen");
            yield return new WaitForSeconds(10);
            gameManager.LoadLevel(1);
            
        }
    }
}
